const app = angular
    .module("myApp", [])
    .controller('myController', ($scope, $http) => {
        $scope.data = [];
        $scope.start = 0

        $http.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
            .then((data) => {
                return data.data;
            }).then((data) => {
                data.map((id) => {
                    $http.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
                        .then((data) => {
                            $scope.data = $scope.data.concat(data.data)
                        })
                })
            })
            .catch((err) => {
                console.error(err);
            });

        $scope.convertDate = (unixTime) => {
            const givenTime = new Date(unixTime * 1000);
            const currentTime = new Date();
            const timeDifference = currentTime.getTime() - givenTime.getTime();

            if (timeDifference / (1000 * 60 * 60 * 24) >= 1) {
                return `${Math.ceil(timeDifference / (1000 * 60 * 60 * 24))} days ago`;
            } else if (timeDifference / (1000 * 60 * 60) >= 1) {
                return `${Math.ceil(timeDifference / (1000 * 60 * 60))} hours ago`;
            } else if (timeDifference / (1000 * 60) >= 1) {
                return `${Math.ceil(timeDifference / (1000 * 60))} minutes ago`;
            } else {
                return `${Math.ceil(timeDifference / (1000))} seconds ago`;
            }
        }

        $scope.nextPage = () => {
            if ($scope.start < $scope.data.length - 30) {
                $scope.start += 30;
            }
        }

        $scope.previousPage = () => {
            if ($scope.start !== 0){
                $scope.start -= 30
            }
        }

    })